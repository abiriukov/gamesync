#!/usr/bin/python3

import os, subprocess, xml.sax

# Copy the save files from src to dest
def copySaveFiles(src, dest):
    # Form the command
    cmd = 'cp -ruv "%s" "%s"' % (src, dest)

    # Execute the command
    result = subprocess.call(cmd, shell=True)

    if result != 0:
        if result < 0:
            print("Killed by signal", result)
        else:
            print("Command failed with return code ", result)
    else:
        print("Success!")

# Custom XML handler
class GameHandler(xml.sax.ContentHandler):
    def __init__(self):
        self.CurrentData = ""
        self.title = ""
        self.source = ""
        self.destination = ""

    # Call on element start
    def startElement(self, tag, attributes):
        self.CurrentData = tag
        if tag == "game":
            self.title = attributes["title"]

    # Call on element end
    def endElement(self, tag):
        if tag == "game":
            print("Copying " + self.title + " from " + self.source + " to " + self.destination)
            copySaveFiles(self.source, self.destination)
        self.CurrentData = ""

    # Call on character read
    def characters(self, content):
        if self.CurrentData == "source":
            self.source = content
        elif self.CurrentData == "destination":
            self.destination = content

if (__name__ == "__main__"):

    # Create an XML parser
    parser = xml.sax.make_parser()

    # Turn off namespaces
    parser.setFeature(xml.sax.handler.feature_namespaces, 0)

    # Override the default ContextHandler   
    Handler = GameHandler()
    parser.setContentHandler(Handler)

    # Parse the file
    parser.parse(os.path.expanduser('~') + "/gamelist.xml")