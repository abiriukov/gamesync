# gamesync

A script written in Python that I use to backup save files of my games to my Nextcloud server.

Instructions:

1) Give the script execution permission.

chmod +x ./gamesync.py

2) Edit the file gamelist.xml.

Put the game title in the attribute title of a game tag:

<game title="Baldur's Gate II Enhanced Edition">

Put the source path in the source tag:

<source>/home/nost/.local/share/Baldur's Gate II - Enhanced Edition</source>

Put the destination path in the destination tag:

<destination>/home/nost/Nextcloud/Ext/Game Saves and Backups/</destination>

3) Copy gamelist.xml into your user folder.

4) (Optional) Copy the script somewhere in your PATH (e.g. /home/user_name/.local/bin or /usr/local/bin).

5) Execute the script.
